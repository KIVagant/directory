# Address book test project

## Requirements

- php ^7.0

## Installation

Clone this repo and run:

```
php -S localhost:8000 -t public
```

Then run on browser:

- ```http://localhost:8000/```
- ```http://localhost:8000/3```
- ```http://localhost:8000/Piotr```
- ```http://localhost:8000/2/Piotr```
- ```http://localhost:8000/Horacego%2023```
- ```http://localhost:8000/2/504212369```
- ```http://localhost:8000/2/Horacego%2023```

## License

Copyright 2016 Eugene Glotov <kivagant at gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
