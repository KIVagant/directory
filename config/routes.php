<?php
namespace Kivagant;

return [
    'routes' => [
//        [
//            'name' => 'testroute',
//            'path' => '/{id:[0-9]+/}{firstname:[a-z]+/}somestring/{?:[0-9]+/}',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\ProcessRequestMiddleware::class,
//                ],
//            ],
//        ],
//        [
//            'name' => 'full-comparsion',
//            'path' => '/hello/world/',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\ProcessRequestMiddleware::class,
//                ],
//            ],
//        ],
//        [
//            'name' => 'simple-regexp',
//            'path' => '/{:hello/?}{?:world/?}',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\ProcessRequestMiddleware::class,
//                ],
//            ],
//        ],
        [
            'name' => 'get-address',
            'path' => '/{?id:[0-9]+/?}{?address:[a-z]+\s[0-9]+/?}{?name:[a-z]+/?}{?phone:[0-9]+/?}',
            'methods' => ['GET'],
            'options' => [
                'middlewares' => [
                    Middleware\ErrorHandlerMiddleware::class,
                    Middleware\ProcessRequestMiddleware::class,
                    Middleware\PrepareResponseMiddleware::class,
                ],
            ],
        ],

    ],
];
