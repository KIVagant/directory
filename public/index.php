<?php
namespace Kivagant;

use Kivagant\Application\Application;
use Kivagant\Config\Config;
use Kivagant\Router\Router;

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

// Dependency Injection Container should be used here
$config = (new Config())->loadDirectory('config');
$router = new Router();

(new Application($config, $router))->run();