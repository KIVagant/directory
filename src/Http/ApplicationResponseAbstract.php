<?php
namespace Kivagant\Http;

abstract class ApplicationResponseAbstract extends Response implements ApplicationResponseInterface
{
    /**
     * @return self
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
        $this->finishRequest();

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function sendContent()
    {
        echo $this->prepare();

        return $this;
    }

    public function __toString()
    {
        return
            $this->getProtocolLine() . "\r\n"
            . implode("\r\n", $this->getHeadersAsLines())
            . "\r\n\r\n"
            . $this->prepare();
    }

    abstract public function setContent($content);
    abstract protected function prepare();
}