<?php
namespace Kivagant\Http;

/**
 * Warning: This class is just a stub and is NOT immutable. Use another libraries for full interface implementation.
 */
class ArrayResponse extends ApplicationResponseAbstract implements ArrayResponseInterface
{
    /**
     * @var array
     */
    protected $content = [];

    public function __construct($content = [], $status = 200, array $headers = [])
    {
        parent::__construct(null, $status, $headers);
        $this->content = $content;
    }

    /**
     * @param array $content
     * @return $this
     */
    public function setContent($content = [])
    {
        $this->content = (array)$content;

        return $this;
    }

    protected function prepare()
    {
        return var_export($this->content, true);
    }
}