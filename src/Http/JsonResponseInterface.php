<?php
namespace Kivagant\Http;

interface JsonResponseInterface extends ApplicationResponseInterface
{
    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content = null);
}