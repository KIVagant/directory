<?php
namespace Kivagant\Http;

/**
 * Warning: This class is just a stub and is NOT immutable. Use another libraries for full interface implementation.
 */
class XmlResponse extends ApplicationResponseAbstract implements StringResponseInterface, XmlResponseInterface
{
    const ROOT_TAG = 'root';

    protected $xml;
    /**
     * @var array
     */
    protected $content = [];

    public function __construct($content = [], $status = 200, array $headers = [])
    {
        parent::__construct(null, $status, $headers);
        $this->xml = new \SimpleXMLElement('<?xml version="1.0"?><' . self::ROOT_TAG . '/>');
        $this->content = $content;
        $this->withHeader('Content-type', 'application/xhtml+xml');
    }

    /**
     * @param array $content
     * @return $this
     */
    public function setContent($content = [])
    {
        $this->content = (string)$content;

        return $this;
    }

    protected function prepare()
    {
        $this->array_to_xml($this->content, $this->xml);

        return $this->xml->asXML();
    }

    protected function array_to_xml($array, \SimpleXMLElement $xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                } else {
                    $subnode = $xml->addChild("item$key");
                    $this->array_to_xml($value, $subnode);
                }
            } else {
                $xml->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}