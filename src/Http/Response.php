<?php
namespace Kivagant\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Warning: This class is just a stub and is NOT immutable. Use another libraries for full interface implementation.
 */
abstract class Response implements ResponseInterface
{
    use MessageTrait;
    protected $statusCode = 200;

    public function __construct(StreamInterface $body = null, $status = 200, array $headers = [])
    {
        if ($body) {
            $this->withBody($body);
        }
        $this->withStatus($status);
        $this->headers = $headers;
    }

    /**
     * Gets the response status code.
     *
     * The status code is a 3-digit integer result code of the server's attempt
     * to understand and satisfy the request.
     *
     * @return int Status code.
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Return an instance with the specified status code and, optionally, reason phrase.
     *
     * If no reason phrase is specified, implementations MAY choose to default
     * to the RFC 7231 or IANA recommended reason phrase for the response's
     * status code.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated status and reason phrase.
     *
     * @link http://tools.ietf.org/html/rfc7231#section-6
     * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @param int $code The 3-digit integer result code to set.
     * @param string $reasonPhrase The reason phrase to use with the
     *     provided status code; if none is provided, implementations MAY
     *     use the defaults as suggested in the HTTP specification.
     * @return self
     * @throws \InvalidArgumentException For invalid status code arguments.
     */
    public function withStatus($code, $reasonPhrase = 'OK')
    {
        $this->statusCode = $code;
        $this->reasonPhrase = $reasonPhrase;
        return $this;
    }

    /**
     * Gets the response reason phrase associated with the status code.
     *
     * Because a reason phrase is not a required element in a response
     * status line, the reason phrase value MAY be null. Implementations MAY
     * choose to return the default RFC 7231 recommended reason phrase (or those
     * listed in the IANA HTTP Status Code Registry) for the response's
     * status code.
     *
     * @link http://tools.ietf.org/html/rfc7231#section-6
     * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @return string Reason phrase; must return an empty string if none present.
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

    public function getHeadersAsLines()
    {
        $headers = [];
        foreach ($this->headers as $header => $value) {
            $headers[$header] = $this->getHeaderLine($header);
        }

        return $headers;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {

            return $this;
        }

        header($this->getProtocolVersion(), true, $this->statusCode);

        $headers = $this->getHeadersAsLines();
        foreach ($headers as $line) {
            header($line, false);
        }

        return $this;
    }

    public function getProtocolLine()
    {
        return sprintf('HTTP/%s %s %s', $this->getProtocolVersion(), $this->getStatusCode(), $this->getReasonPhrase());
    }

    abstract public function send();

    public function finishRequest()
    {
        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }
    }
}