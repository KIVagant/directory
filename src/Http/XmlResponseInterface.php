<?php
namespace Kivagant\Http;

interface XmlResponseInterface extends ApplicationResponseInterface
{
    /**
     * @param array $content
     * @return $this
     */
    public function setContent($content = []);
}