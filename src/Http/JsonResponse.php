<?php
namespace Kivagant\Http;

/**
 * Warning: This class is just a stub and is NOT immutable. Use another libraries for full interface implementation.
 */
class JsonResponse extends ApplicationResponseAbstract implements StringResponseInterface, JsonResponseInterface
{
    /**
     * @var mixed
     */
    protected $content = null;

    public function __construct($content = null, $status = 200, array $headers = [])
    {
        parent::__construct(null, $status, $headers);
        $this->content = $content;
        $this->withHeader('Content-type', 'application/json');
    }

    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    protected function prepare()
    {
        return json_encode($this->content);
    }
}