<?php
namespace Kivagant\Http;


interface StringResponseInterface extends ApplicationResponseInterface
{
    /**
     * @param string $content
     * @return self
     */
    public function setContent($content = '');

}