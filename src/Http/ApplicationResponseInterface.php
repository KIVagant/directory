<?php
namespace Kivagant\Http;

interface ApplicationResponseInterface
{
    /**
     * @return self
     */
    public function send();

    /**
     * @return string
     */
    public function __toString();

    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @param mixed $content
     * @return self
     */
    public function setContent($content);

    /**
     * @return self
     */
    public function sendContent();
}