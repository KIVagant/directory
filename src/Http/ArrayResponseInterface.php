<?php
namespace Kivagant\Http;

interface ArrayResponseInterface extends ApplicationResponseInterface
{
    /**
     * @param array $content
     * @return $this
     */
    public function setContent($content = []);

}