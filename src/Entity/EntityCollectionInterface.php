<?php
namespace Kivagant\Entity;

interface EntityCollectionInterface
{
    public function append(EntityInterface $entity);
    public function prepend(EntityInterface $entity);
    public function detach(EntityInterface $entity);
    public function remove($position = null);
    public function attachCollection(EntityCollection $collection);
    public function current();
    public function next();
    public function key();
    public function valid();
    public function rewind();
    public function toArray();
    public function count();
}