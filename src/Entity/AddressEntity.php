<?php
namespace Kivagant\Entity;

class AddressEntity implements EntityInterface
{
    protected $id = 0;
    protected $name = '';
    protected $phone = '';
    protected $address = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id = 0): self
    {
        $filter = clone $this;
        $filter->id = (int)$id;

        return $filter;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name = ''): self
    {
        $filter = clone $this;
        $filter->name = (string)$name;

        return $filter;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone($phone = ''): self
    {
        $filter = clone $this;
        $filter->phone = (string)$phone;

        return $filter;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $filter = clone $this;
        $filter->address = (string)$address;

        return $filter;
    }
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'address' => $this->address,
        ];
    }
}