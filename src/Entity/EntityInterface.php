<?php
namespace Kivagant\Entity;

interface EntityInterface
{
    public function getId(): int;

    public function setId($id = 0);
    public function toArray(): array;
}