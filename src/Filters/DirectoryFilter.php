<?php
namespace Kivagant\Filters;

class DirectoryFilter
{
    protected $id = 0;
    protected $name = '';
    protected $phone = '';
    protected $address = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id = 0): self
    {
        $filter = clone $this;
        $filter->id = (int)$id;

        return $filter;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name = ''): self
    {
        $filter = clone $this;
        $filter->name = (string)$name;

        return $filter;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone($phone = ''): self
    {
        $filter = clone $this;
        $filter->phone = (string)$phone;

        return $filter;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $filter = clone $this;
        $filter->address = (string)$address;

        return $filter;
    }

    public function getUuid()
    {

        return md5($this->id . $this->name . $this->phone . $this->address);
    }
}