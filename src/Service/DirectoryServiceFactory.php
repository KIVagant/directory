<?php
namespace Kivagant\Service;

use Kivagant\Factory\FactoryInterface;
use Kivagant\Model\DirectoryModelFactory;

class DirectoryServiceFactory implements FactoryInterface
{
    public function __invoke()
    {
        $model = (new DirectoryModelFactory())->__invoke();
        return new DirectoryService($model);
    }
}