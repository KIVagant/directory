<?php
namespace Kivagant\Service;

use Kivagant\Filters\DirectoryFilter;
use Kivagant\Model\DirectoryModel;

class DirectoryService
{
    /**
     * @var DirectoryModel
     */
    protected $model;

    public function __construct(DirectoryModel $model)
    {
        $this->model = $model;
    }

    public function read(DirectoryFilter $filter)
    {

        return $this->model->read($filter);
    }
}