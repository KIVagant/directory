<?php
namespace Kivagant\Layers\Cache;

interface CacheInterface
{
    public function get(string $key);

    public function add(string $key, $value) : bool;

    public function delete(string $key, $value): bool;

    public function has(string $key): bool;
}