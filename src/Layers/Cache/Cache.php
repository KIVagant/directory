<?php
namespace Kivagant\Layers\Cache;

class Cache implements CacheInterface
{
    protected function installed()
    {
        return function_exists('apc_fetch');
    }
    public function get(string $key)
    {
        if (!$this->installed()) {

            return false;
        }

        return apc_fetch($key);
    }
    public function add(string $key, $value): bool
    {
        if (!$this->installed()) {

            return false;
        }

        return apc_add($key, $value);
    }
    public function delete(string $key, $value): bool
    {
        if (!$this->installed()) {

            return false;
        }

        return apc_delete($key);
    }
    public function has(string $key): bool
    {
        if (!$this->installed()) {

            return false;
        }

        return apc_exists($key);
    }
}