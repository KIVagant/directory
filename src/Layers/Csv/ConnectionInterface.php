<?php
namespace Kivagant\Layers\Csv;

interface ConnectionInterface
{
    public function connect();
    public function disconnect();

    /**
     * @return resource|null
     */
    public function getResource();
    public function isConnected(): bool;

    /**
     * Generator
     */
    public function load();
}