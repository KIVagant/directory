<?php
namespace Kivagant\Layers\Csv;

class Csv implements ConnectionInterface
{
    const LIMIT = 100;
    /**
     * @var resource|null
     */
    protected $resource = null;

    /**
     * @var string
     */
    protected $storagePath;

    public function __construct($storagePath = '')
    {
        $this->storagePath = $storagePath;
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    public function connect()
    {
        if (!$this->isConnected()) {
            $this->resource = fopen($this->storagePath, 'rw');
        }
    }

    public function disconnect()
    {
        if ($this->isConnected()) {
            fclose($this->resource);
        }
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function isConnected(): bool
    {
        return is_resource($this->resource);
    }

    public function load()
    {
        $this->connect();
        $id = 0;
        while (($line = fgetcsv($this->resource)) !== FALSE) {
            yield $id++ => $line;
            if ($id >= self::LIMIT) {
                return;
            }
        }
    }
}