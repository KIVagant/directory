<?php
namespace Kivagant\Application;

use Kivagant\Config\Config;
use Kivagant\Config\ConfigInterface;
use Kivagant\Http\ApplicationResponseInterface;
use Kivagant\Http\ArrayResponse;
use Kivagant\Http\ServerRequest;
use Kivagant\Http\StringResponse;
use Kivagant\Pipeline\PipelineMiddleware;
use Kivagant\Router\Route;
use Kivagant\Router\RouteInterface;
use Kivagant\Router\RouteEmpty;
use Kivagant\Router\Router;
use Kivagant\Router\RouterInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Application implements ApplicationInterface
{
    /**
     * @var ConfigInterface|Config
     */
    protected $config;

    /**
     * @var RouterInterface|Router
     */
    protected $router;

    /**
     * @var RouteInterface|Route
     */
    protected $route;

    /**
     * @var ServerRequestInterface|ServerRequest
     */
    protected $request;

    /**
     * @var ResponseInterface|ArrayResponse
     */
    protected $response;

    public function __construct(ConfigInterface $config, RouterInterface $router)
    {
        $this->config = $config;
        $this->router = $router;
    }

    public function run()
    {
        $this->fillRoutes();
        $this->machRoute(array_key_exists('PATH_INFO', $_SERVER) ? $_SERVER['PATH_INFO'] : '/', $_SERVER['REQUEST_METHOD']);
        $this->requestFactory();
        $this->responseFactory();
        $this->runPipeline();
        $this->sendResponse();
    }

    protected function fillRoutes()
    {
        $routes = $this->config->get('routes', []);
        foreach ($routes as $route) {
            $route = new Route($route['name'], $route['path'], $route['methods'], [], $route['options']);
            $this->router->add($route);
        }
    }

    protected function machRoute($url, $method)
    {
        $this->route = $this->router->match($url, $method);
    }

    protected function requestFactory()
    {
        $headers = $this->parseHeaders($_SERVER);
        $this->request = new ServerRequest($this->route->getParams(), $headers);
        $this->request = $this->request->setRoute($this->route);
    }

    protected function responseFactory()
    {
        if ($this->route instanceof RouteEmpty) {
            $this->response = new StringResponse('Page Not Found');
            $this->response->withStatus(404, 'Not Found');
        } else {
            $this->response = new ArrayResponse();
        }
    }

    protected function runPipeline()
    {
        $pipe = new PipelineMiddleware($this->request, $this->response);
        foreach ($this->route->getOption('middlewares', []) as $middlewareName) {
            $pipe = $pipe->pipe(new $middlewareName);
        }
        $this->response = $pipe->process();
    }

    protected function sendResponse()
    {
        if ($this->response instanceof ApplicationResponseInterface) {
            $this->response->send();
        }
    }

    protected function parseHeaders(array $server)
    {
        $headers = [];
        foreach ($server as $key => $value) {
            if ($value && strpos($key, 'HTTP_') === 0) {
                $name = strtr(substr($key, 5), '_', ' ');
                $name = strtr(ucwords(strtolower($name)), ' ', '-');
                $name = strtolower($name);

                $headers[$name] = array_map('trim', explode(',', $value));
                continue;
            }

            if ($value && strpos($key, 'CONTENT_') === 0) {
                $name = substr($key, 8); // Content-
                $name = 'Content-' . (($name == 'MD5') ? $name : ucfirst(strtolower($name)));
                $name = strtolower($name);
                $headers[$name] = array_map('trim', explode(',', $value));
                continue;
            }
        }

        return $headers;
    }
}