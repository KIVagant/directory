<?php
namespace Kivagant\Application;

interface ApplicationInterface
{
    public function run();
}