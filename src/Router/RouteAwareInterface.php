<?php
namespace Kivagant\Router;

interface RouteAwareInterface
{
    /**
     * @return RouteInterface|Route
     */
    public function getRoute();

    /**
     * @immutable
     * @param RouteInterface|Route $route
     * @return RouteAwareInterface
     */
    public function setRoute(RouteInterface $route);
}