<?php
namespace Kivagant\Router;

trait RouteAwareTrait // implements RouteAwareInterface
{
    /**
     * @var RouteInterface|Route
     */
    protected $route;

    /**
     * @return RouteInterface|Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @immutable
     * @param RouteInterface|Route $route
     * @return self
     */
    public function setRoute(RouteInterface $route)
    {
        $obj = clone $this;
        $obj->route = $route;

        return $obj;
    }
}