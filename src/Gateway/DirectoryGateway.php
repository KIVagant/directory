<?php
namespace Kivagant\Gateway;

use Kivagant\Entity\AddressEntity;
use Kivagant\Entity\EntityCollection;
use Kivagant\Filters\DirectoryFilter;
use Kivagant\Layers\Csv\ConnectionInterface;

class DirectoryGateway
{
    /**
     * @var ConnectionInterface
     */
    protected $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function load(DirectoryFilter $filter)
    {
        $data = new EntityCollection();
        foreach ($this->connection->load() as $id => $fields) {
            if (!$fields) {
                break;
            }
            $entity = (new AddressEntity())
                ->setId($id)
                ->setName(array_key_exists(0, $fields) ? $fields[0] : '')
                ->setPhone(array_key_exists(1, $fields) ? $fields[1] : '')
                ->setAddress(array_key_exists(2, $fields) ? $fields[2] : '')
                ;
            if ($filter->getId() && $filter->getId() !== $entity->getId()) {
                continue;
            }
            if ($filter->getName() && $filter->getName() !== $entity->getName()) {
                continue;
            }
            if ($filter->getPhone() && $filter->getPhone() !== $entity->getPhone()) {
                continue;
            }
            if ($filter->getAddress() && $filter->getAddress() !== $entity->getAddress()) {
                continue;
            }

            $data->append($entity);
        }

        return $data;
    }
}