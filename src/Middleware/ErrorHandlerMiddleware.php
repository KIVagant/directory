<?php
namespace Kivagant\Middleware;

use Kivagant\Http\StringResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class ErrorHandlerMiddleware implements MiddlewareInterface
{
    protected $response;

    protected $request;


    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $this->request = $request;
        $this->response = $response;
        try {
            return $next($this->request, $this->response);
        } catch (\Exception $e) {
            $this->error($e);
        } catch (\Throwable $e) {
            $this->error($e);
        }
    }

    protected function error($e)
    {
        $response = new StringResponse('Internal Error');
        $response->withStatus(503, '503 Internal Error: ' . $e->getMessage());
        $response->send();
    }
}