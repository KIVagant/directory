<?php
namespace Kivagant\Middleware;

use Kivagant\Filters\DirectoryFilter;
use Kivagant\Service\DirectoryServiceFactory;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class ProcessRequestMiddleware implements MiddlewareInterface
{
    protected $service;

    public function __construct()
    {
        // TODO: A DIContainer should be used instead
        $this->service = (new DirectoryServiceFactory())->__invoke();
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $filter = (new DirectoryFilter())
            ->setId($request->getAttribute('id', 0))
            ->setName($request->getAttribute('name', ''))
            ->setPhone($request->getAttribute('phone', ''))
            ->setAddress($request->getAttribute('address', ''))
        ;
        $data = $this->service->read($filter);

        /** @var \Kivagant\Http\ArrayResponse $response */
        $response->setContent($data->toArray());

        return $next($request, $response);
    }
}