<?php
namespace Kivagant\Middleware;

use Kivagant\Http\JsonResponse;
use Kivagant\Http\XmlResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class PrepareResponseMiddleware implements MiddlewareInterface
{
    protected $response;

    protected $request;

    protected $result;

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $this->request = $request;
        $this->response = $response;
        $this->selectResponse();

        return $next($this->request, $this->response);
    }

    protected function selectResponse()
    {
        foreach ($this->request->getHeader('Accept') as $type) {
            switch ($type) {
                case 'application/hal+json':
                    /**
                     * @todo Here could be transformers to JSON HAL
                     * @see http://stateless.co/hal_specification.html
                     */
                case 'application/vnd.api+json':
                    /**
                     * @todo Here could be transformers to JSON API
                     * @see http://jsonapi.org/
                     */
                case 'application/json':
                    $this->transform();
                    $this->response = new JsonResponse(
                        $this->response->getContent(),
                        $this->response->getStatusCode(),
                        $this->response->getHeaders()
                    );
                    return;
                case 'application/xhtml+xml':
                    $this->transform();
                    $this->response = new XmlResponse(
                        (array)$this->response->getContent(),
                        $this->response->getStatusCode(),
                        $this->response->getHeaders()
                    );
                    return;
            }
        }
    }

    protected function transform()
    {
        $this->result = [
            'data' => $this->response->getContent(),
            'request' => $this->request->getAttributes(),
            'route' => $this->request->getRoute()->getName(),
        ];
        $this->setMessage();
        $this->response->setContent($this->result);
    }

    protected function setMessage()
    {
        if ($this->result['data']) {
            $this->result['message'] = [
                'status' => 200,
                'text' => 'Found',
            ];
        } else {
            $this->response->withHeader(404, 'Not Found');
            $this->result['message'] = [
                'status' => 404,
                'text' => 'Not Found',
            ];
        }
    }
}