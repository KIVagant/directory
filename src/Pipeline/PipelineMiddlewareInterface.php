<?php
namespace Kivagant\Pipeline;

use Kivagant\Middleware\MiddlewareInterface;

interface PipelineMiddlewareInterface
{
    public function pipe(MiddlewareInterface $middleware);
    public function process();
    public function __invoke();
}