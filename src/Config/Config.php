<?php
namespace Kivagant\Config;

use Kivagant\Exception\RuntimeException;

class Config implements ConfigInterface
{
    protected $config = [];

    public function __construct(array $config = null)
    {
        if ($config) {
            $this->config = $config;
        }
    }

    public function loadDirectory($directory): ConfigInterface
    {
        if (!is_dir($directory)) {
            throw new RuntimeException('Bad config directory');
        }
        $scanned = array_diff(scandir($directory), ['..', '.']);
        foreach ($scanned as $file) {
            $this->loadFile($directory . DIRECTORY_SEPARATOR . $file);
        }

        return $this;
    }

    public function loadFile($filename): ConfigInterface
    {
        if (!is_readable($filename)) {
            throw new RuntimeException('Bad config path');
        }
        $config = require $filename;
        $this->config = array_merge($this->config, $config);

        return $this;
    }

    public function all()
    {

        return $this->config;
    }

    public function get($route, $default = null)
    {
        $config = $this->config;
        $routes = explode('.', $route);
        $endPath = '';
        foreach ($routes as $item) {
            $endPath .= $item . '.';
            if (!array_key_exists($item, $config)) {
                if (null !== $default) {

                    return $default;
                }
                throw new \RuntimeException('Unknown config route and no default values: ' . $endPath, __LINE__);
            }
            $config = $config[$item];
        }

        return $config;
    }

    /**
     * @param $route
     * @return bool
     */
    public function has($route)
    {
        $config = $this->config;
        $routes = explode('.', $route);
        $endPath = '';
        foreach ($routes as $item) {
            $endPath .= $item . '.';
            if (!array_key_exists($item, $config)) {

                return false;
            }
            $config = $config[$item];
        }

        return true;
    }

    public function filter($route, callable $callback)
    {

        return array_filter($this->get($route), $callback, ARRAY_FILTER_USE_BOTH);
    }
}