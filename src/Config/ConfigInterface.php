<?php
namespace Kivagant\Config;

interface ConfigInterface
{
    public function __construct(array $config = null);
    public function loadDirectory($directory): self;
    public function loadFile($filename): self;
    public function all();
    public function get($route, $default = null);
    public function has($route);
}