<?php
namespace Kivagant\Factory;

interface FactoryInterface
{
    public function __invoke();
}