<?php
namespace Kivagant\Model;

use Kivagant\Filters\DirectoryFilter;
use Kivagant\Gateway\DirectoryGateway;
use Kivagant\Layers\Cache\CacheInterface;

class DirectoryModel
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var DirectoryGateway
     */
    protected $gateway;

    public function __construct(CacheInterface $cache, DirectoryGateway $gateway)
    {
        $this->cache = $cache;
        $this->gateway = $gateway;
    }

    public function read(DirectoryFilter $filter)
    {
        $key = $filter->getUuid();
        $data = $this->cache->get($key);
        if ($data === false) {
            $data = $this->gateway->load($filter);
            $this->cache->add($key, $data);
        }

        return $data;
    }
}