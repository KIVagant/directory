<?php
namespace Kivagant\Model;

use Kivagant\Factory\FactoryInterface;
use Kivagant\Gateway\DirectoryGateway;
use Kivagant\Layers\Cache\Cache;
use Kivagant\Layers\Csv\Csv;

class DirectoryModelFactory implements FactoryInterface
{
    public function __invoke()
    {
        // TODO: DI Container and inject Config from DI
        $cache = new Cache();
        $connection = new Csv('storage/csv/directory.csv');
        $gateway = new DirectoryGateway($connection);

        return new DirectoryModel($cache, $gateway);
    }
}